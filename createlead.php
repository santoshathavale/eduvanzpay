<?php
	include('config.php');
	include('common.php');	

	//LEAD INSERT	
	$data1=log_api_request($conn);	
	print_r($data1);
	die;
	
	function log_api_request($conn)
	{		
            $check_valid_ip=check_ip($conn);
            if(!empty($check_valid_ip))	
            {
                $status='Unauthorized';
                $get_inserted_api_request_id=get_inserted_api_request_id($conn,$status);
                return json_encode($check_valid_ip);
            }
            else
            {
                $status='Authorized';
                $get_inserted_api_request_id=get_inserted_api_request_id($conn,$status);
                if($get_inserted_api_request_id['status']==0)
                {
                    return json_encode($get_inserted_api_request_id);
                }
                else
                { echo "come";exit;
                    if(!empty($get_inserted_api_request_id['data']['api_request_id']))
                    {					
                        $leadResult=insert_update_lead($conn,$get_inserted_api_request_id['data']['lead_id'],$get_inserted_api_request_id['data']['api_request_id']);
                        return $leadResult;					
                    }
                    else
                    {
                        return json_encode(array('status'=>0,'message'=>'Invalid api request id','data'=>array()));
                    }
                }			
            }
	}
	

    function insert_update_lead($conn,$leadID=0,$api_request_id=0)
    {		
            $data = json_decode(file_get_contents('php://input'), true);
            $request_data = isset($data['request_data'])?$data['request_data'] : 0;
            $meta_data = isset($data['meta_data']) ? $data['meta_data'] : 0;
            $leadID = isset($data['lead_id'])?$data['lead_id'] : 0;

            $sql = "select admin_id from ip_tracking where delete_flag=0 and whitelisted_ip='".$_SERVER['REMOTE_ADDR']."' limit 1";
            $result = mysqli_query($conn, $sql);
            $studentId=false;

            //User created ELS Id and maintain in IP tracking table
            $admin_id=mysqli_fetch_row($result)[0];           
            if(!empty($request_data))
            {			
                if(empty($leadID))
                {                            
                    $email=$request_data['borrower'][0]['email'];
                    $mobileNo=$request_data['borrower'][0]['mobile'];
                    
                    $emailExist=getStudentByMail($email,$conn);
                    $mobileExist = getStudentByMobile($mobileNo,$conn);                  
                    
                    
                    
                    $emailFlag = ($emailExist['email'] == $mobileExist['email']) ? '1' : '0';
                    $mobileFlag = ($emailExist['mobile'] == $mobileExist['mobile']) ? '1' : '0';
                  
                    if(($mobileFlag && $emailFlag) || (empty($mobileExist) && empty($emailExist))) 
                    {  
                        
                     if($mobileExist['student_id'])
                     {
                        $studentId = $mobileExist['student_id'];
                     }   
                     
                    if(!empty($request_data['loan_data'][0]['has_coborrower']) && $request_data['loan_data'][0]['has_coborrower']==1)
                    {
                        // INSERT LEAD
                        $leadData_borrower=get_lead_data($request_data,$borrowerType='BO');
                        $check_validation_data_borrower=check_validation($leadData_borrower,$conn,TYPE_BORROWER);

                        $leadData_coborrower=get_lead_data($request_data,$borrowerType='CO');
                        $check_validation_data_coborrower=check_validation($leadData_coborrower,$conn,TYPE_COBORROWER);

                        if(empty($check_validation_data_borrower) && empty($check_validation_data_coborrower))
                        {
                            /****** Insert borrower *****/	
                            $insert_bocoborrower=insert_borrower($conn,$leadData_borrower,$admin_id,$leadID='',$api_request_id, $studentId);

                            //return $insert_borrower;
                            /****** Insert borrower *****/							

                            $get_leadID=json_decode($insert_bocoborrower);						
                            $leadID=$get_leadID->data->lead_id;
                            if(!empty($leadID))
                            {
                                //Call Borrower CIBIL
                                call_bocoborrower_cibil($leadID);
                                //call_bocoborrower_cibil($conn,$leadData_borrower,$leadID,TYPE_BORROWER);
                                call_cibil_algo_check($conn,TYPE_BORROWER,$leadID);

                                /****** Insert co-borrower *****/	 							
                                $insert_bocoborrower=insert_coborrower($conn,$leadData_coborrower,$admin_id,$leadID,$api_request_id);

                                //Call Co Borrower CIBIL
                                $get_co_leadID=json_decode($insert_bocoborrower);						
                                $ColeadID=$get_co_leadID->data->lead_id;
                                if(!empty($ColeadID))
                                {
                                    call_bocoborrower_cibil($ColeadID);
                                    //call_bocoborrower_cibil($conn,$leadData_coborrower,$ColeadID,TYPE_COBORROWER);
                                    call_cibil_algo_check($conn,TYPE_COBORROWER,$ColeadID);
                                    
                                    $get_partner_id=get_partner_id($conn); 
                                    if($get_partner_id['recCount']==1)
                                    {
                                        $lead_source_id=$get_partner_id['lead_source_id'];
                                    }
                                    call_back($conn,$ColeadID,$meta_data,$lead_source_id);
                                    /*************************************ACTIVITY**********************************************/
                                    $instituteName = "select institute_name from institute where institute_id='".$leadData_coborrower['institutes_id']."'" ;
                                    $result = mysqli_query($conn, $instituteName);
                                    $instituteName=mysqli_fetch_assoc($result);
                                    $instituteName = $instituteName['institute_name'];
                                    $leadID=$ColeadID;
                                    $activityParameters =array(
                                        'activity_module' => LEAD, //Constant define under MODULE list
                                        'activity_sub_module' => UPDATED_KYC_DETAILS, //Constant define under SUB MODULE list
                                        'leadId' => !empty($leadID) ? $leadID : '' ,
                                        'session_user_id' => !empty($leadData_coborrower['institutes_id']) ? $leadData_coborrower['institutes_id'] :'',
                                        'session_user_name' => !empty($instituteName) ? $instituteName : '',
                                        'date_time' => date('d-m-Y h:i:s'),
                                        'document_name' => '',
                                        'document_list' => '',
                                        'counsellor_name' => '',
                                        'lead_owner_name' => '',
                                        'amount' => '',
                                        'lender_name' => '',
                                        'sanction_amount' => '',
                                        'downpayment' => '',
                                        'interest' => '',
                                        'discount' => '',
                                        'tenure' => '',
                                        'emi_type' => '',
                                        'comments' => '',
                                        'drop_reason' => '',
                                        'reject_code' => '',
                                        'note' => '',

                                    );
                                    add_track_activity($activityParameters);
                                    /*************************************ACTIVITY**********************************************/
                                }							    
                                return $insert_bocoborrower;  
                                /****** Insert co-borrower *****/
                            }			
                        } // If close for both validation check						
                    }
                    else
                    {
                        // INSERT LEAD
                        $leadData_borrower=get_lead_data($request_data,$borrowerType='BO');
                        $check_validation_data_borrower=check_validation($leadData_borrower,$conn,TYPE_BORROWER);

                        if(empty($check_validation_data_borrower))
                        {
                            /****** Insert borrower *****/	
                            $insert_bocoborrower=insert_borrower($conn,$leadData_borrower,$admin_id,$leadID=0,$api_request_id, $studentId);

                            $get_leadID=json_decode($insert_bocoborrower);						
                            $leadID=$get_leadID->data->lead_id;
                            if(!empty($leadID))
                            {
                              //Call Borrower CIBIL
                              call_bocoborrower_cibil($leadID);
                              //call_bocoborrower_cibil($conn,$leadData_borrower,$leadID,TYPE_BORROWER);
                              call_cibil_algo_check($conn,TYPE_BORROWER,$leadID);
                              
                              $get_partner_id=get_partner_id($conn); 
                              if($get_partner_id['recCount']==1)
                              {
                                $lead_source_id=$get_partner_id['lead_source_id'];
                              }
                                    
                              call_back($conn,$leadID,$meta_data,$lead_source_id);

        /*************************************ACTIVITY**********************************************/
                                $instituteName = "select institute_name from institute where institute_id=".$leadData_borrower['institutes_id']."" ;
                                $result = mysqli_query($conn, $instituteName);
                                $instituteName=mysqli_fetch_assoc($result);
                                $instituteName = $instituteName['institute_name'];
                                $activityParameters =array(
                                    'activity_module' => LEAD, //Constant define under MODULE list
                                    'activity_sub_module' => LEAD_CREATION, //Constant define under SUB MODULE list
                                    'leadId' => !empty($leadID) ? $leadID : '' ,
                                    'session_user_id' => !empty($leadData_borrower['institutes_id']) ? $leadData_borrower['institutes_id'] :'',
                                    'session_user_name' => !empty($instituteName) ? $instituteName : '' ,
                                    'date_time' => date('d-m-Y h:i:s'),
                                    'document_name' => '',
                                    'document_list' => '',
                                    'counsellor_name' => '',
                                    'lead_owner_name' => '',
                                    'amount' => '',
                                    'lender_name' => '',
                                    'sanction_amount' => '',
                                    'downpayment' => '',
                                    'interest' => '',
                                    'discount' => '',
                                    'tenure' => '',
                                    'emi_type' => '',
                                    'comments' => '',
                                    'drop_reason' => '',
                                    'reject_code' => '',
                                    'note' => '',

                                );
                                add_track_activity($activityParameters);
         /*************************************ACTIVITY**********************************************/
                            }	   
                            return $insert_bocoborrower;
                            /****** Insert borrower *****/

                        } // If close for both validation check									
                    }
                    return json_encode(array('status'=>0,
                                             'message'=>'Lead error','data'=>array('meta_data'=>$meta_data,'lead_id'=>$lead_id),
                                             'error'=>array(
                                                            'borrower'=>$check_validation_data_borrower,
                                                            'coborower'=>$check_validation_data_coborrower
                                                           )
                                      ));
                    } // If email or mobile exist
                    elseif($mobileExist) 
                    {                       
                        return json_encode(array('status'=>0,
                                             'message'=>'Lead error','data'=>array('meta_data'=>$meta_data,'lead_id'=>$lead_id),
                                             'error'=>array(
                                                            'borrower'=>array(0=>'This mobile number is already registered with email ' . $mobileExist['email'])
                                                           )));
                        
                    } elseif($emailExist) {                       
                        return json_encode(array('status'=>0,
                                             'message'=>'Lead error','data'=>array('meta_data'=>$meta_data,'lead_id'=>$lead_id),
                                             'error'=>array(                                                           
                                                            'borrower'=>array(0=>'This email id is already registered with mobile '.$emailExist['mobile'])
                                                           )));
                    } else {                       
                        return json_encode(array('status'=>0,
                                             'message'=>'Lead error','data'=>array('meta_data'=>$meta_data,'lead_id'=>$lead_id),
                                             'error'=>array(                                                            
                                                            'borrower'=>array(0=>'Email/Mobile Already Mapped')
                                                           )));
                    }

                } //Empty lead 
                else
                {
                    // Check stage and based on that allow data changes in update api
                    $sqlcheckStage6 = "select count(*) as stage6 from lead_status where fk_lead_id=".$leadID." and current_stage=6 and is_deleted=0";					
                    $result = mysqli_query($conn, $sqlcheckStage6);
                    $sqlcheckStage6=mysqli_fetch_array($result);
                                      
                    $sqlcheckStage5 = "select count(*) as stage5 from lead_status where fk_lead_id=".$leadID." and current_stage=5 and is_deleted=0";					
                    $result = mysqli_query($conn, $sqlcheckStage5);
                    $sqlcheckStage5=mysqli_fetch_array($result);
                    if($sqlcheckStage5[0]['stage5']==0)
                    {
                       $sqlcheckTranches=0;
                    }
                    else 
                    {
                        $sqlcheckTranches = "select count(*) as countTranches from disbursement_tranches where lead_id=".$leadID;					
                        $result = mysqli_query($conn, $sqlcheckTranches);
                        $sqlcheckTranches=mysqli_fetch_array($result);
                        if($sqlcheckTranches[0]['countTranches']>0)
                        {
                            $sqlcheckTranches=$sqlcheckTranches[0]['countTranches'];
                        }
                        else
                        {
                            $sqlcheckTranches=0;
                        }
                    }
                    
                    
                    //echo $sqlcheckStage6[0]['stage6']."=====".$sqlcheckTranches;die;
                    // Check stage and based on that allow data changes in update api
                    if($sqlcheckStage6[0]['stage6']==0 && $sqlcheckTranches==0)
                    { 
                        if(!empty($request_data['loan_data'][0]['has_coborrower']) && $request_data['loan_data'][0]['has_coborrower']==1)
                        {
                            //UPDATE LEAD DATA			
                            $leadData_borrower=get_lead_data($request_data,$borrowerType='BO');
                            $check_validation_data_borrower=check_validation($leadData_borrower,$conn,TYPE_BORROWER);

                            $leadData_coborrower=get_lead_data($request_data,$borrowerType='CO');
                            $check_validation_data_coborrower=check_validation($leadData_coborrower,$conn,TYPE_COBORROWER);
                            if(empty($check_validation_data_borrower) && empty($check_validation_data_coborrower))
                            {			
                                $update_coborrower=update_borrower_coborrower($conn,$leadData_borrower,$admin_id,$leadID,$api_request_id,TYPE_BORROWER);

                                //Call CIBIL
                                call_bocoborrower_cibil($leadID);
                                //call_bocoborrower_cibil($conn,$leadData_borrower,$leadID,TYPE_BORROWER);
                                call_cibil_algo_check($conn,TYPE_BORROWER,$leadID);

                                //Check if data missed of co-borrower at the time of insert
                                $sqlcheckCoborrowerExist = "select count(*) as applicant_count from applicant_details where fk_lead_id=".$leadID." and fk_applicant_type_id=2 and is_deleted=0";					
                                $result = mysqli_query($conn, $sqlcheckCoborrowerExist);
                                $sqlcheckCoborrowerExist=mysqli_fetch_array($result);

                                //Co-borrower insert
                                if((int) $sqlcheckCoborrowerExist['applicant_count']==0)
                                {
                                    $update_coborrower=insert_coborrower($conn,$leadData_coborrower,$admin_id,$leadID,$api_request_id,$studentId);

                                    $get_leadID=json_decode($update_coborrower);						
                                    $ColeadID=$get_leadID->data->lead_id;
                                    if(!empty($ColeadID))
                                    {
                                      //Call CIBIL
                                      call_bocoborrower_cibil($ColeadID);
                                      //call_bocoborrower_cibil($conn,$leadData_coborrower,$ColeadID,TYPE_COBORROWER);
                                      call_cibil_algo_check($conn,TYPE_COBORROWER,$ColeadID);
                                      
                                      $get_partner_id=get_partner_id($conn); 
                                      if($get_partner_id['recCount']==1)
                                      {
                                        $lead_source_id=$get_partner_id['lead_source_id'];
                                      }
                                      call_back($conn,$ColeadID,$meta_data,$lead_source_id);
                                        /*************************************ACTIVITY**********************************************/
                                        $instituteName = "select institute_name from institute where institute_id='".$leadData_coborrower['institutes_id']."'" ;
                                        $result = mysqli_query($conn, $instituteName);
                                        $instituteName=mysqli_fetch_assoc($result);
                                        $instituteName = $instituteName['institute_name'];
                                        $leadID=$ColeadID;
                                        $activityParameters =array(
                                            'activity_module' => LEAD, //Constant define under MODULE list
                                            'activity_sub_module' => UPDATED_KYC_DETAILS, //Constant define under SUB MODULE list
                                            'leadId' => !empty($leadID) ? $leadID : '' ,
                                            'session_user_id' => !empty($leadData_coborrower['institutes_id']) ? $leadData_coborrower['institutes_id'] :'',
                                            'session_user_name' => !empty($instituteName) ? $instituteName : '',
                                            'date_time' => date('d-m-Y h:i:s'),
                                            'document_name' => '',
                                            'document_list' => '',
                                            'counsellor_name' => '',
                                            'lead_owner_name' => '',
                                            'amount' => '',
                                            'lender_name' => '',
                                            'sanction_amount' => '',
                                            'downpayment' => '',
                                            'interest' => '',
                                            'discount' => '',
                                            'tenure' => '',
                                            'emi_type' => '',
                                            'comments' => '',
                                            'drop_reason' => '',
                                            'reject_code' => '',
                                            'note' => '',

                                        );
                                        add_track_activity($activityParameters);
                                        /*************************************ACTIVITY**********************************************/
                                    }
                                }
                                //Co-borrower update
                                else
                                {													
                                    $update_coborrower=update_borrower_coborrower($conn,$leadData_coborrower,$admin_id,$leadID,$api_request_id,TYPE_COBORROWER);

                                    //Call 
                                    call_bocoborrower_cibil($leadID);
                                    //call_bocoborrower_cibil($conn,$leadData_coborrower,$leadID,TYPE_COBORROWER);
                                    call_cibil_algo_check($conn,TYPE_COBORROWER,$leadID);
                                    /*************************************ACTIVITY**********************************************/
                                    $instituteName = "select institute_name from institute where institute_id='".$leadData_coborrower['institutes_id']."'" ;
                                    $result = mysqli_query($conn, $instituteName);
                                    $instituteName=mysqli_fetch_assoc($result);
                                    $instituteName = $instituteName['institute_name'];
                                    $leadID=$leadID;
                                    $activityParameters =array(
                                        'activity_module' => LEAD, //Constant define under MODULE list
                                        'activity_sub_module' => UPDATED_KYC_DETAILS, //Constant define under SUB MODULE list
                                        'leadId' => !empty($leadID) ? $leadID : '' ,
                                        'session_user_id' => !empty($leadData_coborrower['institutes_id']) ? $leadData_coborrower['institutes_id'] :'',
                                        'session_user_name' => !empty($instituteName) ? $instituteName : '',
                                        'date_time' => date('d-m-Y h:i:s'),
                                        'document_name' => '',
                                        'document_list' => '',
                                        'counsellor_name' => '',
                                        'lead_owner_name' => '',
                                        'amount' => '',
                                        'lender_name' => '',
                                        'sanction_amount' => '',
                                        'downpayment' => '',
                                        'interest' => '',
                                        'discount' => '',
                                        'tenure' => '',
                                        'emi_type' => '',
                                        'comments' => '',
                                        'drop_reason' => '',
                                        'reject_code' => '',
                                        'note' => '',

                                    );
                                    add_track_activity($activityParameters);
                                    /*************************************ACTIVITY**********************************************/
                                }	
                                return $update_coborrower;										
                            } 						
                        }
                        else
                        {                       
                            $check_validation_data_borrower=array();
                            $check_validation_data_coborrower=array();

                            //UPDATE LEAD DATA			
                            $leadData_borrower=get_lead_data($request_data,$borrowerType='BO');
                            $check_validation_data_borrower=check_validation($leadData_borrower,$conn,TYPE_BORROWER);

                            if(empty($check_validation_data_borrower))
                            {
                                $update_coborrower=update_borrower_coborrower($conn,$leadData_borrower,$admin_id,$leadID,$api_request_id,TYPE_BORROWER);

                                //Call CIBIL
                                call_bocoborrower_cibil($leadID);
                                //call_bocoborrower_cibil($conn,$leadData_borrower,$leadID,TYPE_BORROWER);
                                call_cibil_algo_check($conn,TYPE_BORROWER,$leadID);
                                /*************************************ACTIVITY**********************************************/
                                $instituteName = "select institute_name from institute where institute_id='".$leadData_borrower['institutes_id']."'" ;
                                $result = mysqli_query($conn, $instituteName);
                                $instituteName=mysqli_fetch_assoc($result);
                                $instituteName = $instituteName['institute_name'];
                                $leadID=$leadID;
                                $activityParameters =array(
                                    'activity_module' => LEAD, //Constant define under MODULE list
                                    'activity_sub_module' => UPDATED_KYC_DETAILS, //Constant define under SUB MODULE list
                                    'leadId' => !empty($leadID) ? $leadID : '' ,
                                    'session_user_id' => !empty($leadData_borrower['institutes_id']) ? $leadData_borrower['institutes_id'] :'',
                                    'session_user_name' => !empty($instituteName) ? $instituteName : '',
                                    'date_time' => date('d-m-Y h:i:s'),
                                    'document_name' => '',
                                    'document_list' => '',
                                    'counsellor_name' => '',
                                    'lead_owner_name' => '',
                                    'amount' => '',
                                    'lender_name' => '',
                                    'sanction_amount' => '',
                                    'downpayment' => '',
                                    'interest' => '',
                                    'discount' => '',
                                    'tenure' => '',
                                    'emi_type' => '',
                                    'comments' => '',
                                    'drop_reason' => '',
                                    'reject_code' => '',
                                    'note' => '',

                                );
                                add_track_activity($activityParameters);
                                /*************************************ACTIVITY**********************************************/
                                return $update_coborrower;	

                            }
                        } 
                        return json_encode(array('status'=>0,
                                                         'message'=>'Lead error','data'=>array('meta_data'=>$meta_data,'lead_id'=>$leadID),
                                                         'error'=>array(
                                                                        'borrower'=>$check_validation_data_borrower,
                                                                        'coborower'=>$check_validation_data_coborrower
                                                                        )
                                            ));	
                    }
                    else 
                    {
                        return json_encode(array('status'=>0,'message'=>'Please contact to system admin','data'=>array('meta_data'=>$meta_data,'lead_id'=>$leadID)));
                    }
                    

                } // Else end of empty lead
            } // Else request_data
            else
            {               
               return json_encode(array('status'=>0,'message'=>'Invalid request data','data'=>array()));
            }
    }
?>
